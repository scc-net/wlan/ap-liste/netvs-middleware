from flask import Blueprint
import requests
from net_suite import app, ModMetaData


# we are assuming, that at the time of loading the app is initializes and the config is loaded
bb_name = 'apliste'
apliste = Blueprint(bb_name, __name__)

version = requests.get('{url}/version'.format(url=app.config.get('APLISTE_SERVER_URL')),
                       cert=(app.config.get('APLISTE_CERT'), app.config.get('APLISTE_KEY'))).json()

METADATA = ModMetaData(name=bb_name, mod_path=__name__, gitlab_url='https://git.scc.kit.edu/scc-net-wlan/ap-liste',
                       printable_name='AP-Liste', version=version,
                       contact_email='wlan@scc.kit.edu')

from . import views
