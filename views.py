import requests
from net_suite import app
from flask import jsonify, request, abort
from net_suite.views import api_login_required

from . import apliste


@apliste.route("/json", methods=["GET"])
@api_login_required
def get_data():
    user = request.environ['beaker.session']['login']
    if not user.has_permission('nd.ro:device'):
        return abort(400)
    data = requests.get('{url}/json'.format(url=app.config.get('APLISTE_SERVER_URL')),
                        cert=(app.config.get('APLISTE_CERT'), app.config.get('APLISTE_KEY'))).json()
    return jsonify(data)
